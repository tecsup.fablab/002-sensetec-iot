import paho.mqtt.client as mqtt
import time
import struct
import numpy
import matplotlib.pyplot as plt


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client.subscribe("$SYS/#")
    client.subscribe("sensetec1/logging")
    client.subscribe("sensetec1/data")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    if msg.topic == "sensetec1/logging":
        print(msg.topic + " " + str(msg.payload))
    else:
        print(msg.topic)
    if msg.topic == "sensetec1/data":
        number_of_bytes = len(msg.payload)
        #print("Received: " + str(number_of_bytes))
        if number_of_bytes%4 != 0:
            print("Bad message")
            return
        number_of_floats = int(number_of_bytes/4)
        sensor_data = []
        for i in range(number_of_floats):
            # Little endian
            single_float_tuple = struct.unpack('<f', msg.payload[i*4:(i+1)*4])
            sensor_data.append(single_float_tuple[0])
        # Append lists of 96 bytes <> 2 samples
        senseTec1Data.append(sensor_data)


def sensetec(seconds):
    global timeVector
    myClient.publish("sensetec1/command", "1")
    time.sleep(seconds)
    myClient.publish("sensetec1/command", "0")
    time.sleep(2)
    print(len(senseTec1Data))
    for i in range(len(senseTec1Data)):
        if len(senseTec1Data[i]) % 12 != 0:
            return
        else:
            for ii in range(int(len(senseTec1Data[i])/12)):
                accelDataX.append(senseTec1Data[i][12 * ii])  # :12 * ii + 3])
                accelDataY.append(senseTec1Data[i][12 * ii + 1])
                accelDataZ.append(senseTec1Data[i][12 * ii + 2])
                gyroDataX.append(senseTec1Data[i][12 * ii + 3])  # :12 * ii + 6])
                gyroDataY.append(senseTec1Data[i][12 * ii + 4])
                gyroDataZ.append(senseTec1Data[i][12 * ii + 5])
                magDataX.append(senseTec1Data[i][12 * ii + 6])  # :12 * ii + 9])
                magDataY.append(senseTec1Data[i][12 * ii + 7])
                magDataZ.append(senseTec1Data[i][12 * ii + 8])
                temp1Data.append(senseTec1Data[i][12 * ii + 9])
                presData.append(senseTec1Data[i][12 * ii + 10])
                temp2Data.append(senseTec1Data[i][12 * ii + 11])
    print(len(accelDataX))
    print(len(accelDataY))
    print(len(accelDataZ))
    print(len(gyroDataX))
    print(len(gyroDataY))
    print(len(gyroDataZ))
    print(len(magDataX))
    print(len(magDataY))
    print(len(magDataZ))
    print(len(temp1Data))
    print(len(presData))
    print(len(temp2Data))

    timeVector = numpy.linspace(0, (len(accelDataX)-1)*samplingPeriod, len(accelDataX))

    # Plot accelerometer data
    fig1, axs1 = plt.subplots(3, 1)
    fig1.canvas.set_window_title("Accelerometer Data")
    fig1.suptitle("Sampling frequency: 25Hz")
    # - X Axis
    axs1[0].plot(timeVector, accelDataX)
    axs1[0].set_ylabel('X Axis (m/s^2)')
    axs1[0].set_xlabel('Time (s)')
    axs1[0].grid()
    # - Y Axis
    axs1[1].plot(timeVector, accelDataY)
    axs1[1].set_ylabel('Y Axis (m/s^2)')
    axs1[0].set_xlabel('Time (s)')
    axs1[1].grid()
    # - Z Axis
    axs1[2].plot(timeVector, accelDataZ)
    axs1[2].set_ylabel('Z Axis (m/s^2)')
    axs1[0].set_xlabel('Time (s)')
    axs1[2].grid()

    # Plot gyroscope data
    fig2, axs2 = plt.subplots(3, 1)
    fig2.canvas.set_window_title("Gyroscope Data")
    fig2.suptitle("Sampling frequency: 25Hz")
    # - X Axis
    axs2[0].plot(timeVector, gyroDataX)
    axs2[0].set_ylabel('X Axis (rad/s)')
    axs2[0].set_xlabel('Time (s)')
    axs2[0].grid()
    # - Y Axis
    axs2[1].plot(timeVector, gyroDataY)
    axs2[1].set_ylabel('Y Axis (rad/s)')
    axs2[0].set_xlabel('Time (s)')
    axs2[1].grid()
    # - Z Axis
    axs2[2].plot(timeVector, gyroDataZ)
    axs2[2].set_ylabel('Z Axis (rad/s)')
    axs2[0].set_xlabel('Time (s)')
    axs2[2].grid()

    # Plot magnetometer data
    fig3, axs3 = plt.subplots(3, 1)
    fig3.canvas.set_window_title("Magnetometer Data")
    fig3.suptitle("Sampling frequency: 25Hz")
    # - X Axis
    axs3[0].plot(timeVector, magDataX)
    axs3[0].set_ylabel('X Axis (uT)')
    axs3[0].set_xlabel('Time (s)')
    axs3[0].grid()
    # - Y Axis
    axs3[1].plot(timeVector, magDataY)
    axs3[1].set_ylabel('Y Axis (uT)')
    axs3[0].set_xlabel('Time (s)')
    axs3[1].grid()
    # - Z Axis
    axs3[2].plot(timeVector, magDataZ)
    axs3[2].set_ylabel('Z Axis (uT)')
    axs3[0].set_xlabel('Time (s)')
    axs3[2].grid()

    # Plot barometer data
    fig4, axs4 = plt.subplots(1, 1)
    fig4.canvas.set_window_title("Barometer Data")
    fig4.suptitle("Sampling frequency: 25Hz")
    # - Data
    axs4.plot(timeVector, presData)
    axs4.set_ylabel('Pressure (Pa)')
    axs4.set_xlabel('Time (s)')
    axs4.grid()

    # Plot temperature data
    fig5, axs5 = plt.subplots(2, 1)
    fig5.canvas.set_window_title("Temperature Data")
    fig5.suptitle("Sampling frequency: 25Hz")
    # - From MPU9250
    axs5[0].plot(timeVector, temp1Data)
    axs5[0].set_ylabel('MPU9250 (degC)')
    axs5[0].set_xlabel('Time (s)')
    axs5[0].grid()
    # - From BMP280
    axs5[1].plot(timeVector, temp2Data)
    axs5[1].set_ylabel('BMP280 (degC)')
    axs5[1].set_xlabel('Time (s)')
    axs5[1].grid()
    plt.show()


def test():
    global myBytes
    myBytes = 4*0.2
    print(myBytes)

myClient = mqtt.Client("PythonClient")
myClient.on_connect = on_connect
myClient.on_message = on_message

samplingPeriod = 40/1000 # 40 milliseconds
senseTec1Data = []
accelDataX = []
accelDataY = []
accelDataZ = []
gyroDataX = []
gyroDataY = []
gyroDataZ = []
magDataX = []
magDataY = []
magDataZ = []
temp1Data = []
presData = []
temp2Data = []

myClient.connect("localhost", 1883, 60)

myClient.loop_start()

sensetec(300)
