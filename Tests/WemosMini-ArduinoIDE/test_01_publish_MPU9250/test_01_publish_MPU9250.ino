// Adafruit IO Publish Example
//
// Adafruit invests time and resources providing this open source code.
// Please support Adafruit and open source hardware by purchasing
// products from Adafruit!
//
// Written by Todd Treece for Adafruit Industries
// Copyright (c) 2016 Adafruit Industries
// Licensed under the MIT license.
//
// All text above must be included in any redistribution.

/************************** Configuration ***********************************/

// edit the config.h tab and enter your Adafruit IO credentials
// and any additional configuration needed for WiFi, cellular,
// or ethernet clients.
#include "config.h"

/************************ Example Starts Here *******************************/

// this int will hold the current count for our sketch
float imuData[9];

// set up the 'counter' feed
AdafruitIO_Feed *mpu9250 = io.feed("mpu9250");

/************************* MPU9250 **************************************/
#include "MPU9250.h"

// an MPU9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68
MPU9250 IMU(Wire,0x68);
int stat;

void setup() {

  // start the serial connection
  Serial.begin(9600);

  // wait for serial monitor to open
  while(! Serial);

  Serial.print("Connecting to Adafruit IO");

  // connect to io.adafruit.com
  io.connect();

  // wait for a connection
  while(io.status() < AIO_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  // we are connected
  Serial.println();
  Serial.println(io.statusText());

  // start communication with IMU 
  stat = IMU.begin();
  if (stat < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(stat);
    while(1) {}
  }
}

void loop() {

  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();

  // Read data
  for(int i=0; i<160; i++){
    IMU.readSensor();
    imuData[0]=IMU.getAccelX_mss();
    imuData[1]=IMU.getAccelY_mss();
    imuData[2]=IMU.getAccelZ_mss();
    delay(50);
  } 
  // save count to the 'counter' feed on Adafruit IO
  Serial.print("sending -> ");
  mpu9250->save("Data");
  delay(100);
  for(int i=0; i<3; i++){
    Serial.print(imuData[i]);
    Serial.print(" ");
    mpu9250->save(imuData[i]);
    delay(100);
  }
  Serial.println("");

  // increment the count by 1
  //count[0]++;

  // Adafruit IO is rate limited for publishing, so a delay is required in
  // between feed->save events. In this example, we will wait three seconds
  // (1000 milliseconds == 1 second) during each loop.
  //delay(8000);

}
