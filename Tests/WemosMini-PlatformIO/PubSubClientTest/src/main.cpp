/*
  SenseTec software

  It acquires data from a MPU9250 and a BMP280. It is connected to a MQTT broker,
  when a publish petition is received, all data is send.

  MQTT configuration
  - Subscribe topics:
    - sensetec1/command:
      A general topic to receive commands.
  - Publish topics:
    - sensetec1/logging
      Used to send general messages.
    - sensetec1/data
      Data to be sent.

  MPU9250 Configuration
  For some unknown reason, the WHO_AM_I register says 0x73, used for MPU9255s, and
  not 0x71, used for MPU9250s.
  This sensor should be configured to read BMP280 automatically.
  Using FIFO is recommended.

  BMP280 configuration
  Again, this sensor had a different address, 0x76, which had to be specified in
  sensor configuration.
  Sensor inner configuration can be optimized, accroding to datasheet, to "Indoor
  Navigation" mode, but, it is not necesary at this stage.

  ------------------------------------------------------------------------------
  TROUBLESHOOTING:
  ------------------------------------------------------------------------------
  - Error: Can't access /dev/ttyUSB*
  In order to find outwhich port WEMOS is connected to, run:
    dmesg
  and find CH341.
  When flashing code using Ubuntu, it was needed to run:
  (replace * with the number of the port)
    sudo chmod -R 777 /dev/ttyUSB*
  each time because the current user was non-root and didn't own the folder /dev/ttyUSB*
  It can be solved by adding the non-root user to the group that owns /dev/ttyUSB*.
  First find the group:
    ls -l /dev/ttyUSB*
  Then add the current non-root user to the group:
  (replace groupname with the name of the group)
    sudo usermod -a -G groupname $USER
  Finally, log out and re-enter.

  ------------------------------------------------------------------------------
  - Data format
  To publish float numbers, a uint8_t pointer is needed. Also, the payload length
  must be specified using a unit16_t variable. Be aware that float numbers (and
  maybe everythnig else) are stores in Little Endian format.
  In the beginning, null characters could not be sent but, when previously specified
  conditions were met, complete data was sent.

  ------------------------------------------------------------------------------

  References:
  - MQTT library
  https://github.com/knolleary/pubsubclient?utm_source=platformio&utm_medium=piohome
  - Ticker library
  https://github.com/sstaub/Ticker?utm_source=platformio&utm_medium=piohome
  - MPU9250 library
  https://github.com/bolderflight/MPU9250?utm_source=platformio&utm_medium=piohome
  - MPU9250 datasheet and register map
  http://43zrtwysvxb2gf29r5o0athu.wpengine.netdna-cdn.com/wp-content/uploads/2015/02/MPU-9250-Datasheet.pdf
  https://www.invensense.com/wp-content/uploads/2015/02/RM-MPU-9250A-00-v1.6.pdf
  - BMP280 library
  https://github.com/adafruit/Adafruit_BMP280_Library?utm_source=platformio&utm_medium=piohome
  - BMP280 datasheet
  https://cdn-shop.adafruit.com/datasheets/BST-BMP280-DS001-11.pdf

  Future improvements:
  - Use MPU9250's FIFO
  - Connect BMP280 to MPU9250's auxiliary I2C
  - Configure BMP280 to "Indoor navigation" mode
  - Use esp8266 hardware timer for data acquisition or figure out a better way to
  handle data acquisition and WiFi connection.

  Daniel Marquina
*/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "Ticker.h"
//#include <Wire.h>
#include "MPU9250.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

// Serial debugging flag
#define serialDebug false

// Functions
void setup_wifi();
void MQTTcallback(char* topic, byte* payload, unsigned int length);
bool reconnect();
void sendBadSensorsWarning();
void acquireData();

// Network configuration variables
const char* ssid = "fablab";
const char* password = "f4bl4bt3csup#";
WiFiClient espClient;

// MQTT configuration variables
const char* mqtt_server = "192.168.33.60";
PubSubClient client(espClient);
long lastReconnectAttempt = 0;
const char* inTopic1 = "sensetec1/command";
const char* outTopic1 = "sensetec1/logging";
const char* outTopic2 = "sensetec1/data";
uint8_t* pointerToSensorsData;
uint16_t singleSensorDataPacketLength_B = 96; // Number of bytes to send per packet

// Data acquisition configuration
bool startAcquisitionFlag = false;
bool acquiringDataFlag = false;
bool sendDataFlag = false;
bool stopDataFlag = false;
uint16_t samplingTime = 40; // ms
Ticker dataAcquistionTicker(acquireData, samplingTime);
uint32_t sensorsDataIndex = 0; // Index where data will be stored
const uint8_t singleSampleLength_F = 12; // 10 from MPU9250, 2 from BMP
const uint16_t maxSampleQuantity = 50; // At most 800 samples (9600 floats) can be stored//50samples->600floats->2400bytes
uint32_t sampleCounter = 0; // Counts how many read operations have been done
uint32_t remainingSamplesToSend_B = 0;

// Sensors variables
Ticker badSensorsTicker(sendBadSensorsWarning, 5000);
// - MPU9250 variables
MPU9250 imu(Wire,0x68);
int imuStat; // status
bool badImuFlag = false;
// - BMP280 variables
Adafruit_BMP280 bmp; // I2C
uint8_t bmpAddress = 0x76;
bool badBmpFlag = false;
// - Data
float sensorsData[600]; // singleSampleLength_F*maxSampleQuantity


// Pins definitions
int led_pin = D4;
int test_pin = D7;

//==============================================================================
// Arduino framework setup() function
//==============================================================================
void setup() {
  pinMode(led_pin, OUTPUT);
  pinMode(test_pin, OUTPUT);
  Serial.begin(115200);

  // WiFi and MQTT configuration
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(MQTTcallback);

  // MPU9250 configuration
  imuStat = imu.begin();
  if (imuStat < 0) {
    badImuFlag = true;
    if (serialDebug){
      Serial.println("IMU initialization unsuccessful");
      Serial.println("Check IMU wiring or try cycling power");
      Serial.print("Status: ");
      Serial.println(imuStat);
    }
  }

  // BMP280 configuration
  if (!bmp.begin(bmpAddress)) {
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    badBmpFlag = true;
  }

  // Start Bad sensors warning ticker
  if (badImuFlag || badBmpFlag){
    badSensorsTicker.start();
  }

  // Variable used to reconnect
  lastReconnectAttempt = 0;
}

//==============================================================================
// Arduino framework loop() function
//==============================================================================
void loop() {
  // Connection loop
  if (!client.connected()) {
    long now = millis();
    if (now - lastReconnectAttempt > 5000) {
      lastReconnectAttempt = now;
      // Attempt to reconnect
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
  } else {
    // Client connected
    client.loop();
  }

  // Upate ticket that sends a bad IMU state warning
  if (badImuFlag || badBmpFlag){
    badSensorsTicker.update();
  }

  // Start data acquisiton
  if (startAcquisitionFlag){
    client.publish(outTopic1, "Starting data acquisition.");
    acquiringDataFlag = true;
    sensorsDataIndex = 0; // Again, just in case
    sampleCounter = 0;
    dataAcquistionTicker.start();
    startAcquisitionFlag = false;
  }

  // Update acquisition data Ticker
  if (acquiringDataFlag){
    dataAcquistionTicker.update();
  }

  // Stop data acquisition...
  if (stopDataFlag){
    dataAcquistionTicker.stop();
    stopDataFlag = false;
  }

  // Send data
  if (sendDataFlag){
    // First stop acquisition
    //dataAcquistionTicker.stop();//-----------------------------------------------------------
    // Send notice
    client.publish(outTopic1, "Beginning transmission...");
    // Restart pointer
    pointerToSensorsData = (uint8_t*)sensorsData;
    remainingSamplesToSend_B = sampleCounter*4*12;
    if (serialDebug){
      Serial.print("Sending ");
      Serial.print(remainingSamplesToSend_B);
      Serial.println(" bytes.");
    }
    // Measure time from here...
    digitalWrite(test_pin, HIGH);
    //client.publish(outTopic2, pointerToSensorsData, singleSensorDataPacketLength_B);
    while(remainingSamplesToSend_B > singleSensorDataPacketLength_B){
      //Send 96 bytes <> 2 samples
      client.publish(outTopic2, pointerToSensorsData, singleSensorDataPacketLength_B);
      // Update pointer
      pointerToSensorsData += singleSensorDataPacketLength_B;
      // Update remainingSamplesToSend_B
      remainingSamplesToSend_B -= singleSensorDataPacketLength_B;
    }
    if (remainingSamplesToSend_B > 0){
      client.publish(outTopic2, pointerToSensorsData, (uint16_t)remainingSamplesToSend_B);
      remainingSamplesToSend_B = 0;
    }
    digitalWrite(test_pin, LOW);
    // To here. Less than 1500usfor 1680 bytes, Yay!
    sendDataFlag = false;
    sampleCounter = 0;
    // acquiringDataFlag = true;//---------------------------------------------------------------------
  }
}

//==============================================================================
// Other Functions/
//==============================================================================
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  if (serialDebug){
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  }

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    if (serialDebug){
      Serial.print(".");
    }
  }

  if (serialDebug){
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

//------------------------------------------------------------------------------
void MQTTcallback(char* topic, byte* payload, unsigned int length) {
  if (serialDebug){
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (unsigned int i = 0; i < length; i++) {
      Serial.print((char)payload[i]);
    }
    Serial.println();
  }

  // Decode command
  // - Start data acquisition
  if (payload[0] == '1'){
    startAcquisitionFlag = true;
  }

  // - Stop data acquisition and send
  if (payload[0] == '0'){
    acquiringDataFlag = false;
    sendDataFlag = true;
    stopDataFlag = true;
  }
}

//------------------------------------------------------------------------------
bool reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    if (serialDebug){
      Serial.print("Attempting MQTT connection...");
    }
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      if (serialDebug){
        Serial.println("connected");
      }
      // Once connected, publish an announcement...
      client.publish(outTopic1, "Connected!");
      // ... and resubscribe
      client.subscribe(inTopic1);
    } else {
      if (serialDebug){
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
      }
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
  return client.connected();
}

//------------------------------------------------------------------------------
void sendBadSensorsWarning(){
  if (badImuFlag){
    client.publish(outTopic1, "Bad MPU9250 state. Check SenseTec device.");
  }
  if (badBmpFlag){
    client.publish(outTopic1, "Bad BMP280 state. Check SenseTec device.");
  }
}

//------------------------------------------------------------------------------
void acquireData() {
  digitalWrite(led_pin, HIGH);
  // Acquire Data
  if (acquiringDataFlag){
    // Read MPU9250
    imu.readSensor();
    sensorsData[sensorsDataIndex] = imu.getAccelX_mss();//sensorsDataIndex;//imu.getAccelX_mss();
    sensorsData[sensorsDataIndex + 1] = imu.getAccelY_mss();//sensorsDataIndex + 1;//imu.getAccelY_mss();
    sensorsData[sensorsDataIndex + 2] = imu.getAccelZ_mss();//sensorsDataIndex + 2;//imu.getAccelZ_mss();
    sensorsData[sensorsDataIndex + 3] = imu.getGyroX_rads();//sensorsDataIndex + 3;//imu.getGyroX_rads();
    sensorsData[sensorsDataIndex + 4] = imu.getGyroY_rads();//sensorsDataIndex + 4;//imu.getGyroY_rads();
    sensorsData[sensorsDataIndex + 5] = imu.getGyroZ_rads();//sensorsDataIndex + 5;//imu.getGyroZ_rads();
    sensorsData[sensorsDataIndex + 6] = imu.getMagX_uT();//sensorsDataIndex + 6;//imu.getMagX_uT();
    sensorsData[sensorsDataIndex + 7] = imu.getMagY_uT();//sensorsDataIndex + 7;//imu.getMagY_uT();
    sensorsData[sensorsDataIndex + 8] = imu.getMagZ_uT();//sensorsDataIndex + 8;//imu.getMagZ_uT();
    sensorsData[sensorsDataIndex + 9] = imu.getTemperature_C();//sensorsDataIndex + 9;//imu.getTemperature_C();
    // Time until here: 750 us

    // Read BMP280
    sensorsData[sensorsDataIndex + 10] = bmp.readPressure();//sensorsDataIndex + 10;//bmp.readPressure();
    sensorsData[sensorsDataIndex + 11] = bmp.readTemperature();//sensorsDataIndex + 11;//bmp.readTemperature();
    // Time until here: 1500 us

    if (serialDebug){
      Serial.print(sensorsData[sensorsDataIndex],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 1],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 2],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 3],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 4],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 5],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 6],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 7],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 8],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 9],6);
      Serial.print("\t");
      Serial.print(sensorsData[sensorsDataIndex + 10],6);
      Serial.print("\t");
      Serial.println(sensorsData[sensorsDataIndex + 11],6);
    }

    // Update samples counter
    sampleCounter++;

    // Stop acquisition if maximum number of samples reached
    // - Data will be sent
    if (sampleCounter >= maxSampleQuantity){
      sensorsDataIndex = 0;
      //acquiringDataFlag = false;//----------------------------------------------------------
      sendDataFlag = true;
      if (serialDebug){
        Serial.println("800 samples taken.");
      }
    }
    // Update index
    else{
      sensorsDataIndex += singleSampleLength_F;
    }
    // Time until here, when using serialDebug: 3500 us
  }
  digitalWrite(led_pin, LOW);
}
